# README #

Utilizzo

il seguente path sulla macchina (lo script è predisposto per lavorare con il seguente path)

/opt/apps/scripts

Posizionare i file all'interno del path creato

Copiare il file eseguibile "benchmark.sh" in /usr/local/bin
Dare i permessi di esecuzione al file eseguibile: 'chmod +x /usr/local/bin/benchmark.sh
Configurare il file benchmark.conf con le opzioni desiderate
Testare subito o mettere in cron come da seguente esempio:

* 10,22 * * * /usr/local/bin/benchmark.sh -c

Argomenti

-i --interactive: Consente di eseguire il benchmark in maniere interattiva
-c --config: Esegue il benchmark sulla base delle configurazioni presenti nel file benchmark.conf

!!! Importante: per eseguire il benchmark di MYSQL con sysbench assicurarsi di aver opportunamente eseguito il file 'benchmark_prepare.sh'