#Utilizzare i seguenti comandi per preparare, effettuare e pulire i dati di un benchmark nelle versioni 0.5 e 0.4

#SYSBENCH 0.5 RHEL / CENTOS
sysbench --test=/usr/share/doc/sysbench/tests/db/oltp.lua --oltp-table-size=1000000 --mysql-db=benchmark --mysql-user=benchmark --mysql-password=benchmark prepare
sysbench --test=/usr/share/doc/sysbench/tests/db/oltp.lua --oltp-table-size=1000000 --mysql-db=benchmark --mysql-user=benchmark --mysql-password=benchmark --max-time=60 --oltp-read-only=on --max-requests=0 --num-threads=8 run
sysbench --test=/usr/share/doc/sysbench/tests/db/oltp.lua --mysql-db=benchmark --mysql-user=benchmark --mysql-password=benchmark cleanup

#SYSBENCH 0.4 RHEL / CENTOS
sysbench --test=oltp --oltp-table-size=1000000 --db-driver=mysql --mysql-db=benchmark --mysql-user=benchmark --mysql-password=benchmark prepare
sysbench --test=oltp --oltp-table-size=1000000 --db-driver=mysql --mysql-db=benchmark --mysql-user=benchmark --mysql-password=benchmark --max-time=60 --oltp-read-only=on --max-requests=0 --num-threads=8 run
sysbench --test=oltp --db-driver=mysql --mysql-db=benchmark --mysql-user=benchmark --mysql-password=benchmark cleanup