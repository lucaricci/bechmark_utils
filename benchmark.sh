#!/bin/bash

echo "uid is ${UID}"
echo "user is ${USER}"
echo "username is ${USERNAME}"
BENCH_PATH="/opt/apps/scripts"
LOGFILE="$BENCH_PATH/benchmark.log"
SYSCONF_FILE="$BENCH_PATH/benchmark.conf"

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

for i in "$@"
do

case $i in
    -i|--interactive)
	echo "Proseguo in interactive mode"
		
		#Scelta del benchmark: SysBench o AB
		echo "Scegliere il tipo di benchmark: [(1)ab / (2)sys]"
		read bench_type
		case $bench_type in
		1)
		
		read -p "Inizio con un benchmark di Apache. Premere invio per continuare"
	
		#Setto le var
		echo "Specificare la url"
		read url
		echo "Specificare in numero di richieste"
		read reqs
		echo "Specificare in numero di connessioni"
		read conns
		echo "Specificare il numero di round da eseguire"
		read rounds
		START=1
		END=$rounds
			for (( c=START; c<=END; c++ ))
			do
				if [ -f "$LOGFILE" ]  
					then   
					touch "$LOGFILE"
				fi
				echo "###AB TEST '$(date)' #####" >> "$LOGFILE"
				ab -n "$reqs" -c"$conns" "$url" >> "$LOGFILE"
			done
		;;
		2)
		echo "Inizio con un benchmark di Mysql, assicurarsi di aver creato correttamente il database di test."
		read -p "Premere invio per continuare:"
		
		if [ -f $SYSCONF_FILE ] 
			then
				source  "$SYSCONF_FILE"
				echo "###SYSBENCH TEST '$(date)' #####" >> "$LOGFILE"
				$SYSCOMMAND >> "$LOGFILE"
			else
				echo "File di configurazione ($SYSCONF_FILE) non trovato"
			exit 1
		fi
		;;
		esac
    shift
    ;;
    -c|--config)
    echo "Proseguo in automatico con il file: $SYSCONF_FILE"
		#Scelta del benchmark: SysBench o AB
	echo "Leggo i parametri per il test AB"
		if [ -f $SYSCONF_FILE ] 
			then
				source  $SYSCONF_FILE
				echo "Url= $url"
				echo "Num. rich.= $reqs"
				echo "Num. rich.= $conns"
				echo "Num. conns.= $conns"
				echo "Num. rounds.= $rounds"
				START=1
				END=$rounds
					for (( c=START; c<=END; c++ ))
					do
						if [ -f $LOGFILE ]  
						then   
						touch $LOGFILE
						fi
					echo "Eseguo il test AB"
					echo "###AB TEST '$(date)' #####" >> "$LOGFILE"
					ab -n "$reqs" -c"$conns" "$url" >> "$LOGFILE"
					done
				echo "Eseguo il benchmark su mysql"
				echo "###SYSBENCH TEST '$(date)' #####" >> "$LOGFILE"
				$SYSCOMMAND >> "$LOGFILE"
			else
				echo "File di configurazione ($SYSCONF_FILE) non trovato"
			exit 1
		fi
		;;
    *)
            echo "opzione sconosciuta"
    ;;
esac
done

if [ -z "$i" ]; then
    echo "Utilizzo: -i --interactive -c --config" 1>&2
    exit 2
fi 