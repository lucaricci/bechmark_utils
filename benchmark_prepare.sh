#!/bin/bash
  

MYSQL=$(which mysql) 
  
Q1="CREATE DATABASE IF NOT EXISTS benchmark;"
Q2="GRANT ALL PRIVILEGES ON benchmark.* TO 'benchmark'@localhost IDENTIFIED BY 'benchmark';"
Q3="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}"

read -p "Per poter eseguire il primo blocco lo script richiede la admin pwd di MYSQL. Premi invio per continuare"
  
$MYSQL -uroot -p -e "$SQL"

echo "Preparazione oggetti per il benchmark di mysql"
echo "Scegliere la versione di sysbench: [1 - 0.5 / 2 - 0.4]"
read sysbench_ver
		
case $sysbench_ver in
		1)
		echo "Praparo il seguente statement per il test con sysbench 0.5"
		echo "sysbench --test=/usr/share/doc/sysbench/tests/db/oltp.lua --oltp-table-size=1000000 --mysql-db=benchmark --mysql-user=benchmark --mysql-password=benchmark prepare"
		read -p "Premere un invio per continuare"
		sysbench --test=/usr/share/doc/sysbench/tests/db/oltp.lua --oltp-table-size=1000000 --mysql-db=benchmark --mysql-user=benchmark --mysql-password=benchmark prepare
		;;
		2)
		echo "Praparo il seguente statement per il test con sysbench 0.4"
		echo "sysbench --test=oltp --oltp-table-size=1000000 --db-driver=mysql --mysql-db=benchmark --mysql-user=benchmark --mysql-password=benchmark prepare"
		read -p "Premere un invio per continuare"
		sysbench --test=oltp --oltp-table-size=1000000 --db-driver=mysql --mysql-db=benchmark --mysql-user=benchmark --mysql-password=benchmark prepare
		;;
esac